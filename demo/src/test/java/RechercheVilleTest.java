import org.junit.jupiter.api.Test;

import com.example.NotFoundException;
import com.example.RechercheVille;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

public class RechercheVilleTest {

    @Test
    void testSearchTermLessThanTwoCharacters() {
        RechercheVille rechercheVille = new RechercheVille();
        NotFoundException thrown = assertThrows(NotFoundException.class, () -> {
            rechercheVille.rechercher("A");
        }, "Expected rechercher() to throw, but it didn't");
        assertTrue(thrown.getMessage().contains("Search term must be at least 2 characters long"));
    }
    
    @Test
    void testSearchTermWithTwoOrMoreCharacters() {
        RechercheVille rechercheVille = new RechercheVille();
        List<String> result = rechercheVille.rechercher("Va");
        assertTrue(result.contains("Valence"));
        assertTrue(result.contains("Valenciennes"));
        assertEquals(2, result.size());
    }

    @Test
    void testSearchTermAsPartOfCityName() {
        RechercheVille rechercheVille = new RechercheVille();
        List<String> result = rechercheVille.rechercher("ape");
        assertTrue(result.contains("Budapest"));
        assertEquals(1, result.size());
    }


}
