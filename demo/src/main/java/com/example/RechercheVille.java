package com.example;
import java.util.List;
import java.util.stream.Collectors;

public class RechercheVille {

    private List<String> villes;

    public RechercheVille() {
        villes = List.of("Paris", "Budapest", "Skopje", "Rotterdam", "Valence", "Valenciennes",
                         "Amsterdam", "Vienne", "Sydney", "New York", "Londres", "Bangkok",
                         "Hong Kong", "Dubaï", "Rome", "Istanbul");
    }

    public List<String> rechercher(String mot) {
        if (mot.length() < 2) {
            throw new NotFoundException("Search term must be at least 2 characters long");
        }
        return villes.stream()
                     .filter(ville -> ville.toLowerCase().startsWith(mot.toLowerCase()))
                     .collect(Collectors.toList());
    }
}

